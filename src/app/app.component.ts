import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Jeu de Carte By AG';
  colors = [];
  ranks = [];
  cards:any;
  sortedCards:any;

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  onClickLoadColors() {
    // Send Http request
    this.http
      .get(
        'http://localhost:8080/game/color'
      )
      .subscribe(responseData => {
        console.log(responseData);
        // @ts-ignore
        this.colors =  responseData;
        //resort card
        if (this.sortedCards != null){
          console.log('not null');
          this.onClickLoadSortedCards()
        }

      });
  }


  onClickLoadRanks() {
    // Send Http request
    this.http
      .get(
        'http://localhost:8080/game/rank'
      )
      .subscribe(responseData => {
        console.log(responseData);
        // @ts-ignore
        this.ranks =  responseData;
        //resort card
        if (this.sortedCards != null){
          console.log('not null');
          this.onClickLoadSortedCards()
        }

      });
  }

  onClickLoadCards() {
// Send Http request
    this.http
      .get(
        'http://localhost:8080/game/card?numberOfCards=10'
      )
      .subscribe(responseData => {
        console.log(responseData);
        // @ts-ignore
        this.cards =  responseData;
        //resort card
        if (this.sortedCards != null){
          console.log('not null');
          this.onClickLoadSortedCards()
        }

      });
  }

  onClickLoadSortedCards() {
    // Send Http request
    this.http
      .post(
        'http://localhost:8080/game/card?colors='+this.colors+'&ranks='+this.ranks,
        this.cards
      )
      .subscribe(responseData => {
        console.log(responseData);
        this.sortedCards = responseData;
      });
  }

  showSort() {
    return this.ranks != null && this.colors != null && this.cards != null;
  }
}
